
# REST API for accessing the services provided
REST API for accessing the services provided by the Workflow Service system.

To access the services of rest api assuming you are a user you have to get a token using the following command:

<CLIENT_ID> and <CLIENT_SECRET> must be provided by the authentication service administrator.

`ACCESS_TOKEN=$(curl -d grant_type=password -d username=<YOUR_USERNAME> -d password=<YOUR_PASSWORD> -d client_id=<CLIENT_ID> -d client_secret=<CLIENT_SECRET>   --request POST "https://gitlab.repository.dev-greecevscorona.iit.demokritos.gr/oauth/token" | grep -Po '"access_token":.*?[^\\]"' | cut -d ':' -f2 | tr -d '"')`

<CLIENT_ID> and <CLIENT_SECRET> must be provided by the authentication service administrator.


# API Endpoints

**/submit_workflow**
----
  
  At <b>/submit_workflow</b>, you can submit a new workflow 

* **URL**

  `/submit_workflow`

* **Method:**
  
  `POST` 
*  **URL Params**

   **Required:**
 
   `ref_number=[alphanumeric]` Referral number for the workflow  
   `workflow_type=[alphanumeric]` Type of the workflow  
   **Optional:**
    No optional parameters need to be provided
* **Data Params**  
   For each workflow_type, we must provide specific input files.   
   For each input file, we must provide parameters of the following form: `<workflow_type>-<input_file>`  
   Input files for every workflow type are shown in the table below.

    | workflow_type      | input_file       |
    | -------------------| -----------------|
    | inab-certh         | fastq1 AND fastq2|


    


* **Sample Call:**
  ```
    curl -X POST -H "Authorization:Bearer $ACCESS_TOKEN" https://rest.workflow-service.dev-greecevscorona.iit.demokritos.gr/submit_workflow -F ref_number=2242 -F workflow_type=inab-certh -F inab-certh-fastq1=@sampleID_R1_001.fastq.gz -F inab-certh-fastq2=@sampleID_R2_001.fastq.gz
  ```

* **Success Response:**
  
  <!-- <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_> -->

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
        {
            "message":"successful workflow submission for referral number 2242",
            "status":"succeeded"
        }
    }
    ```
* **Error Response:**

  <!-- <_Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be._> -->

  * **Code:** 404 <br />
    **Content:** 
    ```json
    {
        {
        "status":"error",
        "code":404,
        "name":"Not Found",
        "message":"No workflow with workflow_type=inab-certh could be found. Check /list_workflows for the available workflow types"
        }
    }
    ```

    OR

  * **Code:** 403 <br />
    **Content:** 
    ```json
    {
        {
        "status":"error",
        "code":403,
        "name":"Forbidden",
        "message":"cannot use referral number 2242 because it was used by another group"
        }
    }
    ```
  * **Code:** 403 <br />
    **Content:** 
    ```json
    {
        {
        "status":"error",
        "code":403,
        "name":"Forbidden",
        "message":"cannot use referral number 2242 because it is related to a workflow with status Succeeded"
        }
    }
    ```


**workflow_metadata**
----
  
  At <b>/workflow_metadata</b>, you can retrieve metadata for a workflow that belongs to your group. 

* **URL**

  `/workflow_metadata`

* **Method:**
  
  `GET` 
*  **URL Params**

   **Required:**
 
   `ref_number=[alphanumeric]`

   **Optional:**
 
   No optional parameters need to be provided

* **Data Params**

  No data parameters need to be provided

  <!-- <_If making a post request, what should the body payload look like? URL Params rules apply here too._> -->


* **Sample Call:**

  ```
    curl -X GET -H "Authorization:Bearer $ACCESS_TOKEN" https://rest.workflow-service.dev-greecevscorona.iit.demokritos.gr/workflow_metadata?ref_number=2242
  ```



* **Success Response:**
  
  <!-- <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_> -->

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
        "message":"found workflow with referral number 2242",
        "result": {
            "finished_at":"2022-03-11T19:53:10.734000",
            "group":"organization-group-inab-certh",
            "status":"Succeeded",
            "submitted_at":"2022-03-11T19:30:16.310000",
            "user":"inab-certh-tester",
            "workflow_type":"inab-certh"
            },
        "status":"ok"
    }
    ```
* **Error Response:**

  <!-- <_Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be._> -->

  * **Code:** 400 <br />
    **Content:** 
    ```json
    {
        {
        "status":"error",
        "code":400,
        "name":"Bad Request",
        "message":"parameter ref_number must be provided"
        }
    }
    ```

  OR

  * **Code:** 404  <br />
    **Content:**
    ```json
    {
        {
        "status":"error",
        "code":404,
        "name":"Not Found",
        "message":"no workflows found with referral number <ref_number>"
        }
    }
   
    ```

  OR

  * **Code:** 403  <br />
    **Content:** 
    ```json
    {
        {
        "status":"error",
        "code":403,
        "name":"Forbidden",
        "message":"workflow with referral number <refNumber> belongs to another group"
        }
    }
    ```
    

**/group_workflow_metadata**
----
  
  At <b>/group_workflow_metadata</b>, you can retrieve metadata for every workflow that belongs to your group. Results are sorted by submitted_at datetime 

* **URL**

  `/group_workflow_metadata`

* **Method:**
  
  `GET` 
*  **URL Params**

   **Required:**
 
   No required parameters

   **Optional:**
 
   `workflow_type=[alphanumeric]` Get metadata only for specific `<worfklow_type>`  
   `status=[alphanumeric]` Get metadata only for specific `<status> `  
   `user=[alphanumeric]` Get metadata for workflows that were submitted by specific `<user>`  
   `limit=[number]` Get metadata for the first `<limit>` workflows  

* **Data Params**

  No data parameters need to be provided

  <!-- <_If making a post request, what should the body payload look like? URL Params rules apply here too._> -->


* **Sample Call:**

```
    curl -X GET -H "Authorization:Bearer $ACCESS_TOKEN" 'https://rest.workflow-service.dev-greecevscorona.iit.demokritos.gr/group_workflow_metadata?workflow_type=inab-certh&user=inab-certh-tester'
```


* **Success Response:**
  
  <!-- <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_> -->

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
        "message":"found workflows for group organization-group-inab-certh",
        "results":[
            {
                "finished_at":"2022-03-11T19:53:10.734000",
                "group":"organization-group-inab-certh",
                "status":"Succeeded",
                "submitted_at":"2022-03-11T19:30:16.310000",
                "user":"inab-certh-tester",
                "workflow_type":"inab-certh"
            },
            {
                "finished_at":"2022-03-11T14:50:19.163000",
                "group":"organization-group-inab-certh",
                "status":"Succeeded",
                "submitted_at":"2022-03-11T14:27:02.998000",
                "user":"inab-certh-tester",
                "workflow_type":"inab-certh"
            },
            {
                "finished_at":"2022-03-11T13:54:32.954000",
                "group":"organization-group-inab-certh",
                "status":"Succeeded",
                "submitted_at":"2022-03-11T13:30:11.456000",
                "user":"inab-certh-tester",
                "workflow_type":"inab-certh"
            }
        ],
        "status":"ok"
    }
    ```
* **Error Response:**

  <!-- <_Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be._> -->

  * **Code:** 404 <br />
    **Content:** 
    ```json
    {
        {
        "status":"error",
        "code":400,
        "name":"Bad Request",
        "message":"no workflows found for query "
        }
    }
    ```




**/outputs**
----
  
  At <b>/outputs</b>, you can download the output files (in zip format) that were produced by a workflow. Workflow must have been successfully completed in order to retrieve output files.

* **URL**

  `/outputs`

* **Method:**
  
  `GET` 
*  **URL Params**

   **Required:**
 
   `ref_number=[alphanumeric]` Referral number for the workflow  
   
   **Optional:**
    No optional parameters need to be provided
* **Data Params**  
    No data parameters need to be provided

* **Sample Call:**
  ```
     curl -X GET -H "Authorization:Bearer $ACCESS_TOKEN" https://rest.workflow-service.dev-greecevscorona.iit.demokritos.gr/outputs?ref_number=2242 -o outputs.zip
  ```

* **Success Response:**
    * **Code:** 200 <br />
     **Content:** 
        `outputs.zip` file that contains output files 
* **Error Response:**
    * **Code:** 404 <br />
     **Content:** 
    ```json
     {
        {
            "status":"error",
            "code":404,
            "name":"Not Found",
            "message":"no workflows found with referral number 2242"
        }
     }
    ```

    OR
  
    * **Code:** 403 <br />
     **Content:** 
    ```json
     {
        {
            "status":"error",
            "code":403,
            "name":"Forbidden",
            "message":"workflow with referral number 2242 belongs to another group"
        }
     }
    ```

    OR

    * **Code:** 403 <br />
     **Content:** 
    ```json
     {
        {
        "status":"error",
        "code":403,
        "name":"Forbidden",
        "message":"workflow with referral number 2242 must have \"Succeeded\" status in order to get output files "
        }
     }
    ```


**/inputs**
----
  
  At <b>/inputs</b>, you can download the input files (in zip format) that were produced by a workflow. Workflow must have been successfully completed in order to retrieve output files.

* **URL**

  `/inputs`

* **Method:**
  
  `GET` 
*  **URL Params**

   **Required:**
 
   `ref_number=[alphanumeric]` Referral number for the workflow  
   
   **Optional:**
    No optional parameters need to be provided
* **Data Params**  
    No data parameters need to be provided

* **Sample Call:**
  ```
     curl -X GET -H "Authorization:Bearer $ACCESS_TOKEN" https://rest.workflow-service.dev-greecevscorona.iit.demokritos.gr/inputs?ref_number=2242 -o inputs.zip
  ```

* **Success Response:**
    * **Code:** 200 <br />
     **Content:** 
        `inputs.zip` file that contains output files 
* **Error Response:**
    * **Code:** 404 <br />
     **Content:** 
    ```json
     {
        {
            "status":"error",
            "code":404,
            "name":"Not Found",
            "message":"no workflows found with referral number 2242"
        }
     }
    ```

    OR
  
    * **Code:** 403 <br />
     **Content:** 
    ```json
     {
        {
            "status":"error",
            "code":403,
            "name":"Forbidden",
            "message":"workflow with referral number 2242 belongs to another group"
        }
     }
    ```

    OR

    * **Code:** 403 <br />
     **Content:** 
    ```json
     {
        {
        "status":"error",
        "code":403,
        "name":"Forbidden",
        "message":"workflow with referral number 2242 must have \"Succeeded\" status in order to get output files "
        }
     }
    ```


**/list_workflows**
----
  
  At <b>/list_workflows<\b>, you can retrieve every supported workflow type
* **URL**

  `/list_workflows`

* **Method:**
  
  `GET` 
*  **URL Params**

   **Required:**
 
   No required parameters

   **Optional:**

   No optional parameters need to be provided
 
* **Data Params**

  No data parameters need to be provided



* **Sample Call:**

  ```
    curl -X GET -H "Authorization:Bearer $ACCESS_TOKEN" https://rest.workflow-service.dev-greecevscorona.iit.demokritos.gr/list_workflows
  ```


* **Success Response:**
  
  <!-- <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_> -->

  * **Code:** 200 <br />
    **Content:** 
    ```json  
    {
    
        "workflow_types":[
            "inab-certh"
        ]
    
    }
    ```

