FROM python:3.10.2-alpine3.15

ENV GUNICORN_WORKERS=1
ENV GUNICORN_THREADS=4

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY app.py run.sh config.json ./
COPY routines /app/routines

CMD /app/run.sh
