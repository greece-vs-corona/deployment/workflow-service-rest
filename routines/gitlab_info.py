import requests
import os


def get_user_information(access_token):
    """
    returns a dictionary with user's information
    currently, we include "id", "username","name" and "email" of a user

    :param access_token: the access token by oidc provider
    :return: dict containing "id", "username","name" and "email"
    """
    headers = {"Authorization": "Bearer " + str(access_token)}
    response = requests.get(os.environ.get('GITLAB_USERINFO_URL'), headers=headers).json()
    user_information = {
        "id": response["id"],
        "username": response["username"],
        "name": response["name"],
        "email": response["email"]
    }
    return user_information


def get_groups(access_token):
    """
    returns a list which contains the groups of a user

    :param access_token: the access token by oidc provider
    :return: list containing user groups
    """
    headers = {"Authorization": "Bearer " + str(access_token)}
    response = requests.get(os.environ.get('GITLAB_GROUPS_URL'), headers=headers).json()

    groups = []
    for group in response:
        groups.append(group["name"])

    return groups
