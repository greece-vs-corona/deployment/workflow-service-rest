import ast
import logging
import os

from flask import abort

from routines.gitlab_info import get_groups, get_user_information


def get_access_token(request):
    """ returns the access token from the request"""
    return request.headers.get('Authorization').split(' ')[1]


def check_allowed_user(access_token):
    """
    checks if a user belongs in the allowed groups based on the provided access token and aborts otherwise

    :param access_token: the access token by oidc provider
    """
    groups_list = get_groups(access_token)

    allowed_groups = ast.literal_eval(os.environ.get('ALLOWED_GROUPS'))

    if not list(set(groups_list) & set(allowed_groups)):
        abort(403, description='you are not a member of an allowed group')


def get_organization_group(access_token):
    """
    returns the user organization group starting but not equal with 'organization-group-', otherwise aborts with 403

    :param access_token: the access token by oidc provider
    :return: user's organization group
    """

    group_prefix = 'organization-group-'

    user = get_user_information(access_token)['username']

    groups = get_groups(access_token)

    groups_list = [i for i in groups if i.startswith(group_prefix) and i != group_prefix]

    if len(groups_list) > 1:
        logging.error(f'user {user} is member of multiple groups starting with "{group_prefix}"')
        abort(403, description=f'users that are members to multiple groups starting with "{group_prefix}" are not allowed')

    if len(groups_list) == 0:
        logging.error(f'user {user} is not a member of a group starting with with "{group_prefix}"')
        abort(403, description=f'you are not a member of a group starting with with "{group_prefix}"')

    return groups_list[0]
