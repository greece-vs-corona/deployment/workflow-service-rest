import logging
import os
import shutil
from os.path import basename
from zipfile import ZipFile

import boto3
from flask import request, abort
from pymongo import MongoClient
import uuid
from routines.helpers import check_allowed_user, get_access_token, get_organization_group

mongo_host = os.getenv('MONGO_HOST')
mongo_port = int(os.getenv('MONGO_PORT'))
mongo_username = os.getenv('MONGO_USER')
mongo_password = os.getenv('MONGO_PASS')
mongo_authSource = os.getenv('MONGO_AUTHENTICATION_SOURCE')
mongo_database = os.getenv('MONGO_DATABASE')
mongo_collection = os.getenv('MONGO_COLLECTION')

logging.basicConfig(level=logging.DEBUG)


def download_files_from_s3(app, mode):
    """
    creates a zip containing the requested files, returns the zip to the client and the deletes it.

    parameters:
      "app": the flask app
      "mode": must be 'inputs' or 'outputs'
    """
    access_token = get_access_token(request)

    check_allowed_user(access_token)

    group = get_organization_group(access_token)

    referral_num = request.args.get('ref_number', default=None, type=str)

    if referral_num is None:
        abort(400, description='parameter "ref_number" must be provided')

    with MongoClient(host=mongo_host, port=mongo_port, username=mongo_username, password=mongo_password, authSource=mongo_authSource) as mongo_client:

        db = mongo_client[mongo_database]
        collection = db[mongo_collection]

        result = collection.find_one({'referral_num': referral_num}, {
            '_id': 0,
            'group': 1,
            'status': 1,
            'inputs': 1,
            'outputs': 1
        })

        if result is None:
            abort(404, description=f'no workflows found with referral number "{referral_num}"')

        if result['group'] != group:
            abort(402, description=f'workflow with referral number "{referral_num}" belongs to another group')

        if result['status'] != "Succeeded":
            abort(403, description=f'workflow with referral number "{referral_num}" must have "Succeeded" status in order to get "{mode}" files')

    s3_endpoint_url = os.environ.get('S3_ENDPOINT_URL')
    s3_access_key = os.environ.get('S3_ACCESS_KEY')
    s3_secret_key = os.environ.get('S3_SECRET_KEY')
    s3 = boto3.resource('s3', endpoint_url=s3_endpoint_url, aws_access_key_id=s3_access_key, aws_secret_access_key=s3_secret_key)

    download_directory = os.path.join(os.environ.get("DOWNLOADS_PATH"), str(uuid.uuid4()))

    logging.debug(f'creating directory: "{download_directory}" for referral_num: "{referral_num}" and mode: "{mode}"')

    os.makedirs(download_directory, mode=0o777)

    paths = result[mode]

    for path in paths:
        split_path = path.split('/')
        bucket_name = split_path[2]

        prefix = ""
        for i in range(3, len(split_path)):
            prefix += split_path[i]
            if i != len(split_path)-1:
                prefix += "/"

        bucket = s3.Bucket(bucket_name)

        for s3_file in bucket.objects.filter(Prefix=prefix):
            file_object = s3_file.key
            file_name = str(file_object.split('/')[-1])
            logging.debug(f'Downloading file "{file_object}"')
            bucket.download_file(file_object, os.path.join(download_directory, file_name))

    zip_name = mode + ".zip"
    zip_path = os.path.join(download_directory, zip_name)
    with ZipFile(zip_path, 'w') as zipObj:
        for folderName, _, filenames in os.walk(download_directory):
            for filename in filenames:
                if filename != zip_name:
                    file_path = os.path.join(folderName, filename)
                    zipObj.write(file_path, basename(file_path))
                    os.unlink(os.path.join(download_directory, filename))

    def generate():
        with open(zip_path, "rb") as f:
            while True:
                data = f.read(1024)
                if not data:
                    break
                yield data
        try:
            os.remove(zip_path)
            shutil.rmtree(download_directory)
        except Exception as err:
            logging.error(err)

    r = app.response_class(generate(), mimetype='application/zip')
    r.headers.set('Content-Disposition', 'attachment', filename=zip_name)
    return r
