import json
import logging
import os
import urllib.request
import requests

import argo_workflows
from argo_workflows.api import workflow_service_api
from argo_workflows.model.io_argoproj_workflow_v1alpha1_arguments import IoArgoprojWorkflowV1alpha1Arguments
from argo_workflows.model.io_argoproj_workflow_v1alpha1_parameter import IoArgoprojWorkflowV1alpha1Parameter
from argo_workflows.model.io_argoproj_workflow_v1alpha1_workflow import IoArgoprojWorkflowV1alpha1Workflow
from argo_workflows.model.io_argoproj_workflow_v1alpha1_workflow_create_request import IoArgoprojWorkflowV1alpha1WorkflowCreateRequest
from argo_workflows.model.io_argoproj_workflow_v1alpha1_workflow_spec import IoArgoprojWorkflowV1alpha1WorkflowSpec
from argo_workflows.model.io_argoproj_workflow_v1alpha1_workflow_template_ref import IoArgoprojWorkflowV1alpha1WorkflowTemplateRef
from argo_workflows.model.object_meta import ObjectMeta
from flask import abort
from werkzeug.utils import secure_filename

logging.basicConfig(level=logging.DEBUG)


class WorkflowException(Exception):
    pass


def check_workflow_parameters(request, workflow_type):
    """ checks if workflow_type is valid and all parameters for that type are provided, aborts otherwise"""

    urls, input_parameters = extract_workflow_information(workflow_type)
    if urls is None:
        abort(404, description=f'No workflow with workflow_type={workflow_type} could be found. Check /list_workflows for the available workflow types')

    # first pass to check if all required files exist in the given request
    if "sample_id" not in request.form.keys():
        for input_parameter in input_parameters:
            filename = "".join(list(input_parameter.keys()))
            necessity = "".join(list(input_parameter.values()))
            if necessity == "required":
                # if no input files were provided or if a file is missing
                if not request.files or request.files[filename] is None:
                    logging.debug(f'no input file or url found for required parameter "{filename}"')
                    abort(409, description=f'aborted. no input file or url found for required parameter "{filename}"')


def create_input_files(request, referral_num, workflow_type):
    """ save input files in proper directory and create a relevant inputs.yml for the workflow"""
    urls, input_parameters = extract_workflow_information(workflow_type)

    current_directory = os.path.join(os.environ.get('UPLOADS_PATH'), referral_num)
    yaml_path = current_directory + "/inputs.yml"

    if not os.path.exists(current_directory):
        os.mkdir(current_directory, 0o777)

    yaml_url = urls["yml"]
    urllib.request.urlretrieve(yaml_url, yaml_path)
    # replace placeholders with actual paths
    with open(yaml_path, 'r') as file:
        file_content = file.read()

    if "sample_id" in request.form.keys():
        filenames=["".join(list(input_parameter.keys())) for input_parameter in input_parameters]
 
        ena_url = "https://www.ebi.ac.uk/ena/portal/api/filereport?accession=" + str(request.form["sample_id"]) + "&result=read_run&fields=fastq_ftp&format=json&download=false"
        r = requests.get(url=ena_url)
        data = r.json()[0]
        fastq_files = data["fastq_ftp"].split(';')
        
        i = 0
        for file_url in fastq_files:            
            secure_file = secure_filename(file_url)
            file_path = os.path.join(current_directory, secure_file)
            urllib.request.urlretrieve("ftp://"+file_url, file_path)
            placeholder = "<" + filenames[i] + ">"
            file_content = file_content.replace(placeholder, os.path.join(current_directory, secure_file))
            i += 1
    else:
        for input_parameter in input_parameters:
            filename = "".join(list(input_parameter.keys()))
            necessity = "".join(list(input_parameter.values()))
            if necessity == "required":
                secure_file = secure_filename(request.files[filename].filename)
                request.files[filename].save(os.path.join(current_directory, secure_file))
                placeholder = "<" + filename + ">"
                file_content = file_content.replace(placeholder, os.path.join(current_directory, secure_file))
            elif necessity == "optional":
                secure_file = secure_filename(request.files[filename].filename)
                request.files[filename].save(os.path.join(current_directory, secure_file))
                placeholder = "<" + filename + ">"
                file_content = file_content.replace(placeholder, os.path.join(current_directory, secure_file))

        with open(yaml_path, 'w') as file:
            file.write(file_content)


def extract_workflow_information(workflow_type):
    with open("/app/config.json", "r") as fp:
        config = json.load(fp=fp)

    workflows = config["workflows"]
    urls = {}
    for workflow_name in workflows:

        if workflow_name == workflow_type:
            # may add more information here in the future based on config.json structure
            urls['cwl'] = workflows[workflow_name]["cwl"]
            urls['yml'] = workflows[workflow_name]["yml"]
            input_parameters = workflows[workflow_name]["input_parameters"]
            return urls, input_parameters

    return None, None


def submit_argo_workflow(user, email, group, cwl_url, ref_number, workflow_type):
    try:
        configuration = argo_workflows.Configuration(host="https://argo-server:2746")
        configuration.verify_ssl = False

        api_client = argo_workflows.ApiClient(configuration)
        api_instance = workflow_service_api.WorkflowServiceApi(api_client)

        manifest = IoArgoprojWorkflowV1alpha1Workflow(
            metadata=ObjectMeta(
                generate_name=workflow_type+'-'+ref_number+'-',
                labels={
                    'user': user,
                    'group': group,
                    'type': workflow_type,
                    'referral_num': ref_number
                }
            ),
            spec=IoArgoprojWorkflowV1alpha1WorkflowSpec(
                arguments=IoArgoprojWorkflowV1alpha1Arguments(
                    parameters=[IoArgoprojWorkflowV1alpha1Parameter(
                        name='group',
                        value=group
                    ), IoArgoprojWorkflowV1alpha1Parameter(
                        name='email',
                        value=email
                    ), IoArgoprojWorkflowV1alpha1Parameter(
                        name='referral_num',
                        value=ref_number
                    ),IoArgoprojWorkflowV1alpha1Parameter(
                        name='workflow_type',
                        value=workflow_type
                    ), IoArgoprojWorkflowV1alpha1Parameter(
                        name='cwl_url',
                        value=cwl_url
                    )]
                ),
                workflow_template_ref=IoArgoprojWorkflowV1alpha1WorkflowTemplateRef(name='greecevscorona-workflow-template')
            )
        )

        logging.debug(f'submitting manifest for referral number "{ref_number}": {str(manifest)}')

        api_response = api_instance.create_workflow(
            namespace='argo',
            body=IoArgoprojWorkflowV1alpha1WorkflowCreateRequest(workflow=manifest),
            _check_return_type=False)

        str_api_response = str(api_response)

        logging.debug(f'api_response for referral number "{ref_number}": {str_api_response}')

        return str_api_response

    except Exception as e:
        logging.exception(e)
        raise WorkflowException("Something went wrong while submitting the workflow")
