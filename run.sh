#!/bin/sh

gunicorn -b 0.0.0.0:8000 app:app --workers=$GUNICORN_WORKERS --threads=$GUNICORN_THREADS --timeout 8000 --log-level debug
