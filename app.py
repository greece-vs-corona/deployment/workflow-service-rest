import datetime
import json
import logging
import os
import shutil

from flask import Flask, request, abort
from flask_oidc import OpenIDConnect
from pymongo import MongoClient, IndexModel, DESCENDING
from pymongo.errors import DuplicateKeyError
from werkzeug.exceptions import HTTPException

from routines.download_files import download_files_from_s3
from routines.gitlab_info import get_user_information
from routines.helpers import check_allowed_user, get_access_token, get_organization_group
from routines.submit_workflow import check_workflow_parameters, create_input_files, extract_workflow_information, submit_argo_workflow, WorkflowException

mongo_host = os.getenv('MONGO_HOST')
mongo_port = int(os.getenv('MONGO_PORT'))
mongo_username = os.getenv('MONGO_USER')
mongo_password = os.getenv('MONGO_PASS')
mongo_authSource = os.getenv('MONGO_AUTHENTICATION_SOURCE')
mongo_database = os.getenv('MONGO_DATABASE')
mongo_collection = os.getenv('MONGO_COLLECTION')

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)

app.config.update({
    'OIDC_CLIENT_SECRETS': '/etc/oidc_client_secrets/client_secrets.json',
    'SECRET_KEY': os.environ.get('FLASK_SECRET_KEY'),
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
})


@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        'status': 'error',
        "code": e.code,
        "name": e.name,
        "message": e.description,
    })
    response.content_type = "application/json"
    return response


oidc = OpenIDConnect(app)


@app.route('/dummy', methods=['GET', 'POST'])
@oidc.accept_token(require_token=True)
def dummy():
    access_token = get_access_token(request)

    check_allowed_user(access_token)

    if request.method == 'POST':

        return {"status": "Passed authentication phase with POST request"}
    else:
        return {"status": "Passed authentication phase with GET request"}


@app.route('/get_configuration', methods=['GET'])
@oidc.accept_token(require_token=False)
def get_configuration():
    with open('/app/config.json', 'r') as file:
        data = file.read()
    return {"config": data}


@app.route('/submit_workflow', methods=['POST'])
@oidc.accept_token(require_token=True)
def submit_workflow():
    access_token = get_access_token(request)

    check_allowed_user(access_token)

    ref_number_param = request.form['ref_number']

    if ref_number_param is None:
        abort(400, description='"ref_number" parameter must be provided')

    referral_num = str(ref_number_param)
    if referral_num == "":
        abort(400, description='"ref_number" parameter cant be empty string')

    workflow_type_param = request.form['workflow_type']

    if workflow_type_param is None:
        abort(400, description='"workflow_type" parameter must be provided')

    workflow_type = str(workflow_type_param)

    user_info = get_user_information(access_token)

    user = user_info["username"]

    email = user_info["email"]

    group = get_organization_group(access_token)

    # check if workflow_type is valid and all parameters for that type are provided or abort
    check_workflow_parameters(request=request, workflow_type=workflow_type)

    with MongoClient(host=mongo_host, port=mongo_port, username=mongo_username, password=mongo_password, authSource=mongo_authSource) as mongo_client:

        mongo_db = mongo_client[mongo_database]
        mongo_coll = mongo_db[mongo_collection]

        # check if workflow with this ref num has been already submitted in the past
        workflow = mongo_coll.find_one({'referral_num': referral_num}, {'_id': 0, 'group': 1, 'status': 1})

        # if it has been already submitted
        if workflow is not None:

            # if workflow has been submitted by another group, abort
            if workflow['group'] != group:
                logging.debug(f'user "{user}" tried to use referral number "{referral_num}" already used by another group')
                abort(403, description=f'cannot use referral number "{referral_num}" because it was used by another group')

            # if workflow has been completed successfully then abort, otherwise delete the previous run
            workflow_status = workflow['status']

            if workflow_status != 'Failed' and workflow_status != 'Error':
                logging.debug(f'user "{user}" tried to use referral number "{referral_num}" with status "{workflow_status}"')
                abort(403, description=f'cannot use referral number "{referral_num}" because it is related to a workflow with status "{workflow_status}"')

            logging.debug(f'user "{user}" retrying workflow with referral number "{referral_num}" and status "{workflow_status}"')

            count = mongo_coll.delete_many({'referral_num': referral_num, 'status': {"$in": ['Failed', 'Error']}})

            if count.deleted_count == 0:
                logging.debug(f'deleted 0 documents for referral number "{referral_num}"')
                abort(409, description=f'aborted. are you submitting the same referral number "{referral_num}" from another client?')

        # indices should be created on database level upon init. this is added as an extra check.
        ref_index = IndexModel('referral_num', name='referral_num_unique', unique=True)
        group_index = IndexModel('group')
        mongo_coll.create_indexes([ref_index, group_index])

        logging.debug(f'user "{user}" submitting workflow with referral number "{referral_num}"')

        # submit the new workflow with status "Submitting"
        try:
            mongo_coll.insert_one(
                {'referral_num': referral_num,
                 'user': user,
                 'group': group,
                 'status': 'Submitting',
                 'submitted_at': datetime.datetime.utcnow(),
                 'workflow_type': workflow_type
                 })
        except DuplicateKeyError as e:
            # this should never happen
            logging.exception(e)
            abort(500, description='something went wrong, please try again')

        # store input files and prepare inputs.yml
        create_input_files(request=request, referral_num=referral_num, workflow_type=workflow_type)

        urls, _ = extract_workflow_information(workflow_type)
        cwl_url = urls["cwl"]

        try:
            response = submit_argo_workflow(user, email, group, cwl_url, referral_num, workflow_type)

            mongo_coll.update_one({'referral_num': referral_num}, {"$set": {'workflow_api_response': response}})

            logging.debug(f'successful workflow submission by user "{user}" for referral number "{referral_num}"')
            return {'status': 'succeeded', 'message': f'successful workflow submission for referral number "{referral_num}"'}, 200

        except WorkflowException as e:

            logging.exception(e)

            current_directory = os.path.join(os.environ.get('UPLOADS_PATH'), referral_num)
            shutil.rmtree(current_directory)

            mongo_coll.update_one(
                {'referral_num': referral_num},
                {"$set": {
                    'status': 'Error',
                    'failures': str(e),
                    'finished_at': datetime.datetime.utcnow()}})

            abort(500, description='something went wrong while submitting the workflow. please try again. if the error persist notify the administrators.')

    return {'status': 'ok', 'message': 'workflow was successfully submitted'}, 200


@app.route('/workflow_metadata', methods=['GET'])
@oidc.accept_token(require_token=True)
def status():
    access_token = get_access_token(request)

    check_allowed_user(access_token)

    group = get_organization_group(access_token)

    referral_num = request.args.get('ref_number', default=None, type=str)

    if referral_num is None:
        abort(400, description='parameter "ref_number" must be provided')

    with MongoClient(host=mongo_host, port=mongo_port, username=mongo_username, password=mongo_password, authSource=mongo_authSource) as mongo_client:

        db = mongo_client[mongo_database]
        collection = db[mongo_collection]

        result = collection.find_one({'referral_num': referral_num}, {
            '_id': 0,
            'user': 1,
            'group': 1,
            'status': 1,
            'workflow_type': 1,
            'submitted_at': 1,
            'finished_at': 1,
            'failures': 1
        })

        if result is None:
            abort(404, description=f'no workflows found with referral number "{referral_num}"')

        if result['group'] != group:
            abort(403, description=f'workflow with referral number "{referral_num}" belongs to another group')

        result['submitted_at'] = result['submitted_at'].isoformat()

        try:
            result['finished_at'] = result['finished_at'].isoformat()
        except KeyError:
            pass

    return {'status': 'ok', 'message': f'found workflow with referral number "{referral_num}"', 'result': result}, 200


@app.route('/group_workflow_metadata', methods=['GET'])
@oidc.accept_token(require_token=True)
def group_workflow_metadata():
    """ 
    returns workflow metadata for user's group, sorted by submitted_at datatime.

    parameters: 
      "workflow_type": filter by workflow type
      "status": filter by status
      "user": filter by user
      "limit": limit the results
    """

    access_token = get_access_token(request)

    check_allowed_user(access_token)

    group = get_organization_group(access_token)

    with MongoClient(host=mongo_host, port=mongo_port, username=mongo_username, password=mongo_password, authSource=mongo_authSource) as mongo_client:

        db = mongo_client[mongo_database]
        collection = db[mongo_collection]

        query = {'group': group}

        projection = {
            '_id': 0,
            'referral_num': 1,
            'workflow_type': 1,
            'status': 1,
            'user': 1,
            'group': 1,
            'submitted_at': 1,
            'finished_at': 1,
            'failures': 1
        }

        workflow_type = request.args.get('workflow_type', default=None, type=str)

        if workflow_type is not None:
            query['workflow_type'] = workflow_type

        user = request.args.get('user', default=None, type=str)

        if user is not None:
            query['user'] = user

        workflow_status = request.args.get('status', default=None, type=str)

        if workflow_status is not None:
            query['status'] = workflow_status

        limit = request.args.get('limit', default=None, type=int)

        if limit is None:
            results = collection.find(query, projection).sort('submitted_at', DESCENDING)
        else:
            results = collection.find(query, projection).sort('submitted_at', DESCENDING).limit(limit)

        if collection.count_documents(query) == 0:
            abort(404, description=f'no workflows found for query "{query}"')

        metadata = []
        for result in results:

            result['submitted_at'] = result['submitted_at'].isoformat()
            try:
                result['finished_at'] = result['finished_at'].isoformat()
            except KeyError:
                pass

            metadata.append(result)

        return {'status': 'ok', 'message': f'found workflows for group "{group}"', 'results': metadata}, 200


@app.route('/list_workflows', methods=['GET'])
def list_available_workflows():
    with open("/app/config.json", "r") as fp:
        config = json.load(fp=fp)

    workflows = config["workflows"]
    workflows_list = []
    for workflow_type in workflows:
        workflows_list.append(workflow_type)

    return {'workflow_types': workflows_list}


@app.route('/outputs', methods=['GET'])
@oidc.accept_token(require_token=True)
def outputs():
    return download_files_from_s3(app=app, mode="outputs")


@app.route('/inputs', methods=['GET'])
@oidc.accept_token(require_token=True)
def inputs():
    return download_files_from_s3(app=app, mode="inputs")


@app.route('/can_be_submitted', methods=['GET'])
@oidc.accept_token(require_token=True)
def check_workflow():
    access_token = get_access_token(request)

    check_allowed_user(access_token)

    user = get_user_information(access_token)["username"]
    group = get_organization_group(access_token)

    referral_num = request.args.get('ref_number', default=None, type=str)

    with MongoClient(host=mongo_host, port=mongo_port, username=mongo_username, password=mongo_password, authSource=mongo_authSource) as mongo_client:

        mongo_db = mongo_client[mongo_database]
        mongo_coll = mongo_db[mongo_collection]

        # check if workflow with this ref num has been already submitted in the past
        workflow = mongo_coll.find_one({'referral_num': referral_num}, {'_id': 0, 'group': 1, 'status': 1})

        # if it has been already submitted
        if workflow is not None:

            # if workflow has been submitted by another group, abort
            if workflow['group'] != group:
                logging.debug(f'user "{user}" tried to use referral number "{referral_num}" already used by another group')
                abort(403, description=f'cannot use referral number "{referral_num}" because it was used by another group')

            # if workflow has been completed successfully then abort, otherwise delete the previous run
            workflow_status = workflow['status']

            if workflow_status != 'Failed' and workflow_status != 'Error':
                logging.debug(f'user "{user}" tried to use referral number "{referral_num}" with status "{workflow_status}"')
                abort(403, description=f'cannot use referral number "{referral_num}" because it is related to a workflow with status "{workflow_status}"')

            logging.debug(f'user "{user}" retrying workflow with referral number "{referral_num}" and status "{workflow_status}"')

            count = mongo_coll.delete_many({'referral_num': referral_num, 'status': {"$in": ['Failed', 'Error']}})

            if count.deleted_count == 0:
                logging.debug(f'deleted 0 documents for referral number "{referral_num}"')
                abort(409, description=f'aborted. are you submitting the same referral number "{referral_num}" from another client?')

        return {"code": 200, "status": "ok"}


if __name__ == '__main__':
    app.run()
